﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.BLL;
using LMS.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpPost("login")]
        [AllowAnonymous]
        public User Login(User user) 
        {
            UserManager userManager = new UserManager();
            user = userManager.ValidateAuthentication(user);
            return user;
        }
        [HttpPost("registration")]
        [AllowAnonymous]
        public User Registration(User user) 
        {
            UserManager userManager = new UserManager();
            UpdateResult updateResult = new UpdateResult();
            updateResult = userManager.UserSave(user);
            return user;
        }
    }
}