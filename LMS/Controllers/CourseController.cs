﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.BLL;
using LMS.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        [HttpPost("saveCourse")]
        [AllowAnonymous]
        public Course SaveCourse(Course Course) 
        {
            CourseManager CourseManager = new CourseManager();
            UpdateResult updateResult = new UpdateResult();
            updateResult = CourseManager.CourseSave(Course);
            return Course;
        }
    }
}