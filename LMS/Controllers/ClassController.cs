﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.BLL;
using LMS.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        [HttpPost("saveClass")]
        [AllowAnonymous]
        public ClassEntity SaveClass(ClassEntity classEntity) 
        {
            ClassManager classManager = new ClassManager();
            UpdateResult updateResult = new UpdateResult();
            updateResult = classManager.ClassSave(classEntity);
            return classEntity;
        }
    }
}