﻿using LMS.CommonLib;
using LMS.DAL;
using LMS.Entities;
using System;
using System.Data;
namespace LMS.BLL
{
    public class ClassManager
    {
        DBHelper dbHelper = new DBHelper();
        public UpdateResult ClassSave(ClassEntity classEntity)
        {
            UpdateResult updateResult = new UpdateResult();
            try
            {
                IDbCommand cmd = dbHelper.GetCommandObject();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ClassSave";
                cmd.Parameters.Add(dbHelper.GetParameterObject("@ClassName", classEntity.ClassName));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@CreatedByID", classEntity.CreatedByID));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@ModifiedByID", classEntity.ModifiedByID));
                int ID = dbHelper.ExecuteScalar(cmd);
                updateResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                updateResult.IsSuccess = false;
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return null;
            }
            finally
            {
                dbHelper.CloseConnection();
            }
            return updateResult;
        }
    }
}
