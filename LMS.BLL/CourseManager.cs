﻿using LMS.CommonLib;
using LMS.DAL;
using LMS.Entities;
using System;
using System.Data;
namespace LMS.BLL
{
    public class CourseManager
    {
        DBHelper dbHelper = new DBHelper();
        public UpdateResult CourseSave(Course Course)
        {
            UpdateResult updateResult = new UpdateResult();
            try
            {
                IDbCommand cmd = dbHelper.GetCommandObject();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CourseSave";
                cmd.Parameters.Add(dbHelper.GetParameterObject("@CourseTitle", Course.CourseTitle));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@CourseDescription", Course.CourseDescription));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@TopicTitle", Course.TopicTitle));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@TopicDescription", Course.TopicDescription));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@TopicAttachment", Course.TopicAttachment));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@CreatedByID", 1));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@ModifiedByID", 0));
                int ID = dbHelper.ExecuteScalar(cmd);
                updateResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                updateResult.IsSuccess = false;
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return null;
            }
            finally
            {
                dbHelper.CloseConnection();
            }
            return updateResult;
        }
    }
}
