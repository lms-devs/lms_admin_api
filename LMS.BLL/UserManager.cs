﻿using LMS.CommonLib;
using LMS.DAL;
using LMS.Entities;
using System;
using System.Data;

namespace LMS.BLL
{
    public class UserManager
    {
        DBHelper dbHelper = new DBHelper();
        public User ValidateAuthentication(User user)
        {
            try
            {
                //user.UserName = user.UserName;
                //user.Password = user.Password;
                IDbCommand cmd = dbHelper.GetCommandObject();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "UserGet";
                cmd.Parameters.Add(dbHelper.GetParameterObject("@UserName", user.UserName)); 
                cmd.Parameters.Add(dbHelper.GetParameterObject("@Password", user.Password));
                user = dbHelper.ExecuteReader(cmd).ConvertToEntity<User>();
            }
            catch (Exception ex)
            {
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return null;
            }
            finally
            {
                dbHelper.CloseConnection();
            }
            return user;
        }
        public UpdateResult UserSave(User user)
        {
            UpdateResult updateResult = new UpdateResult();
            try
            {
                
                IDbCommand cmd = dbHelper.GetCommandObject();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "UserSave";
                cmd.Parameters.Add(dbHelper.GetParameterObject("@UserName", user.UserName));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@Password", user.Password));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@UserType", user.UserType));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@ReferenceID", user.ReferenceID));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@CreatedByID", user.CreatedByID));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@ModifiedByID", user.ModifiedByID));
                cmd.Parameters.Add(dbHelper.GetParameterObject("@IsActive", user.IsActive));
                int ID = dbHelper.ExecuteScalar(cmd);
                updateResult.IsSuccess = true;
            }
            catch (Exception ex)
            {
                updateResult.IsSuccess = false;
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return null;
            }
            finally
            {
                dbHelper.CloseConnection();
            }
            return updateResult;
        }
    }
}
