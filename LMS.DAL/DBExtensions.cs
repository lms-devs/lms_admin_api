﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace LMS.DAL
{
    public static class MyExtensionMethods
    {
        public static IDbCommand AddParameters<T>(this IDbCommand cmd, T entity, DBHelper dbHelper, List<string> excludedProperties = null)
        {

            if (excludedProperties == null)
            {
                excludedProperties = new List<string>();
            }
            excludedProperties.AddRange(new List<string> { "CreatedDate", "ModifiedDate" });
            // cmd.Parameters.Add(dbHelper.GetParameterObject("@Email", candidate.Email));

            PropertyInfo[] pInfos = entity.GetType().GetProperties();
            foreach (PropertyInfo pInfo in pInfos)
            {
                if (!excludedProperties.Contains(pInfo.Name)) //Check for excluded Properties
                {
                    string dataType = "";
                    // We need to check whether the property is NULLABLE
                    if (pInfo.PropertyType.IsGenericType && pInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        // If it is NULLABLE, then get the underlying type. eg if "Nullable<int>" then this will return just "int"
                        dataType = pInfo.PropertyType.GetGenericArguments()[0].Name;
                    }
                    else
                    {
                        dataType = pInfo.PropertyType.Name;
                    }

                    switch (dataType)
                    {
                        //case "DateTime":
                        //    //sqlParameterCollection.Add(new SqlParameter(pInfo.Name, SqlDbType.DateTime));
                        //    //sqlParameterCollection[pInfo.Name].Value = pInfo.GetValue(obj, null);

                        //    if (pInfo.GetValue(entity, null) != null)
                        //    {
                        //        cmd.Parameters.Add(dbHelper.GetParameterObject("@" + pInfo.Name, pInfo.GetValue(entity, null)));
                        //    }
                        //    else
                        //    {
                        //        cmd.Parameters.Add(dbHelper.GetParameterObject("@" + pInfo.Name, DBNull.Value));
                        //    }


                        //    break;

                        default:
                            if (pInfo.GetValue(entity, null) != null)
                            {
                                cmd.Parameters.Add(dbHelper.GetParameterObject("@" + pInfo.Name, pInfo.GetValue(entity, null)));
                            }
                            else
                            {
                                cmd.Parameters.Add(dbHelper.GetParameterObject("@" + pInfo.Name, DBNull.Value));
                            }

                            break;
                    }
                }
            }
            return cmd;
        }
    }
}
