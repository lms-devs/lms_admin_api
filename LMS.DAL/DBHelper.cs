﻿using LMS.CommonLib;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace LMS.DAL
{
    public class DBHelper
    {
        //private string _connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString.ToString();
        private string _connectionString = "Server=DESKTOP-4KOK99S;Database=LMSDatabase;User ID=sa;Password=server.123;Trusted_Connection=True";
        private SqlConnection _sqlConn;

        public DBHelper()
        {
            Log4NetConfiguration.LoadConfiguration();
        }

        public SqlConnection OpenConnection()
        {
            if (_connectionString != string.Empty)
            {
                _sqlConn = new SqlConnection(_connectionString);
                if (_sqlConn != null)
                {
                    if (_sqlConn.State == System.Data.ConnectionState.Closed)
                        _sqlConn.Open();
                }
            }
            return _sqlConn;
        }

        public void CloseConnection()
        {
            if (_sqlConn != null)
                if (_sqlConn.State != System.Data.ConnectionState.Closed)
                    _sqlConn.Close();
        }

        public void DisposeConnection()
        {
            if (_sqlConn != null)
                if (_sqlConn.State != System.Data.ConnectionState.Closed)
                    _sqlConn.Dispose();
        }

        /// <summary>
        /// Get data table containing records for result of provided sql command.
        /// </summary>
        /// <param name="_sqlCmd"></param>
        /// <returns></returns>
        public DataTable GetDataTable(IDbCommand _sqlCmd)
        {
            DataTable oDt = new DataTable();

            try
            {

                using (SqlConnection sqlCon = new SqlConnection(_connectionString))
                {
                    //Open sql connection for sql command.
                    if (_sqlCmd != null)
                    {
                        _sqlCmd.Connection = sqlCon;
                    }

                    //Create data adapter for sql command
                    SqlDataAdapter oDa = new SqlDataAdapter((SqlCommand)_sqlCmd);
                    //Fill records in data table.
                    oDa.Fill(oDt);
                    _sqlCmd = null;
                }

            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            }
            finally
            {
                //CloseConnection();
            }

            return oDt;
        }

        /// <summary>
        /// Executes sql command and returns true/false (comand executed successfully or not)
        /// </summary>
        /// <param name="_sqlCmd"></param>
        /// <returns></returns>
        public Boolean ExecuteQuery(IDbCommand _sqlCmd)
        {
            Boolean oResult = false;

            try
            {
                //Open sql connection for sql command.
                if (_sqlCmd != null)
                {
                    _sqlCmd.Connection = OpenConnection();
                }

                //Execute sql command
                _sqlCmd.ExecuteNonQuery();
                oResult = true;

            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return oResult;

        }

        /// <summary>
        /// Excutes sql command and returns first column of first row from result.
        /// </summary>
        /// <param name="_sqlCmd"></param>
        /// <returns></returns>
        public int ExecuteScalar(IDbCommand _sqlCmd)
        {
            int sResult = 0;

            try
            {
                //Open sql connection for sql command.
                if (_sqlCmd != null)
                {
                    _sqlCmd.Connection = OpenConnection();
                }

                //Execute sql command
                sResult = _sqlCmd.ExecuteScalar().ToIntSafe();

            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            }
            finally
            {
                CloseConnection();
            }
            return sResult;

        }

        public DataSet GetDataset(IDbCommand _sqlCmd)
        {
            DataSet objDataSet = new DataSet();
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(_connectionString))
                {
                    //Open sql connection for sql command.
                    if (_sqlCmd != null)
                    {
                        _sqlCmd.Connection = sqlCon;
                    }
                    //Create data adapter for sql command
                    IDbDataAdapter oDa = new SqlDataAdapter((SqlCommand)_sqlCmd);

                    //Fill records in data table.
                    oDa.Fill(objDataSet);
                    _sqlCmd = null;
                }
            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            }
            finally
            {
                //CloseConnection();
            }
            return objDataSet;
        }

        public IDataReader ExecuteReader(IDbCommand cmd)
        {
            IDataReader result = null;

            try
            {
                //Open sql connection for command.
                if (cmd != null)
                {
                    cmd.Connection = OpenConnection();
                }

                //Execute sql command
                result = cmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            }
            return result;
        }

        public IDbCommand GetCommandObject()
        {
            return new SqlCommand();
        }

        public IDbDataParameter GetParameterObject(string parameterName, object objectValue)
        {
            return new SqlParameter(parameterName, objectValue);
        }
    }
}
