﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entities
{
    public class UpdateResult
    {
        public string Data { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public int InterviewID { get; set; }
    }
}
