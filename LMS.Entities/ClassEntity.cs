﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entities
{
    public class ClassEntity : BaseEntity
    {
        public string ClassName { get; set; }
    }
}
