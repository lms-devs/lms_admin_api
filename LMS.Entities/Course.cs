﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entities
{
    public class Course : BaseEntity
    {
        public string CourseTitle { get; set; }
        public string CourseDescription { get; set; }
        public string TopicTitle { get; set; }
        public string TopicDescription { get; set; }
        public string TopicAttachment { get; set; }
    }
}
