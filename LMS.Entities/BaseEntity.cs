﻿using System;

namespace LMS.Entities
{
    public class BaseEntity
    {
        public int ID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? CreatedByID { get; set; }
        public int? ModifiedByID { get; set; }
        public bool? IsActive { get; set; }
    }
}
