﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public int? ReferenceID { get; set; }
        public string Name { get; set; }
        public bool? RememberMe { get; set; }
    }
}
