﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace LMS.CommonLib
{
    public class MessagingUtility
    {
        #region [Email Methods]

        //Severity	Code	Description	Project	File	Line	Suppression State
        //Warning CS0618	'ConfigurationManager.AppSettings' is obsolete: 'This method is obsolete, it has been replaced by System.Configuration!System.Configuration.ConfigurationManager.AppSettings'	iMentus.iURL.CommonLib D:\Projects\interviewURL\iMentus.iURL\iMentus.iURL.CommonLib\MessagingUtility.cs	28	Active

        private static SmtpClient oSmtpClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"].ToString());

        public static Boolean SendMail(string sFromAddress, string sToAddress, string cc, string bcc, string sSubject, string sBody)
        {
            lock (oSmtpClient)
            {
                Boolean oResult = false;
                string sNetworkUserName = "";
                string sNetworkPassword = "";

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailCredentialUserName"]))
                {
                    sNetworkUserName = ConfigurationManager.AppSettings["EmailCredentialUserName"].ToString();
                }
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailCredentialPassword"]))
                {
                    sNetworkPassword = ConfigurationManager.AppSettings["EmailCredentialPassword"].ToString();
                }

                if (string.IsNullOrEmpty(sFromAddress))
                {
                    sFromAddress = sNetworkUserName;
                }

                if (sNetworkUserName != string.Empty && sNetworkPassword != string.Empty)
                {
                    oSmtpClient.UseDefaultCredentials = false;
                    oSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    oSmtpClient.Credentials = new NetworkCredential(sNetworkUserName, sNetworkPassword);
                    oSmtpClient.EnableSsl = true;
                    oSmtpClient.Port = 587;
                }

                try
                {
                    string sFromDisplayName = "";
                    if (string.IsNullOrEmpty(sFromAddress))
                    {
                        sFromAddress = ConfigurationManager.AppSettings["EmailFromAddress"].ToString();
                    }
                    MailAddress fromAddress = new MailAddress(sFromAddress, sFromDisplayName);
                    MailMessage oMessage = new MailMessage();
                    oMessage.From = fromAddress;
                    List<MailAddress> mailAddressList = new List<MailAddress>();
                    //Get To Mail Address List
                    mailAddressList = GetMailAddressList(sToAddress);
                    if (mailAddressList != null && mailAddressList.Count > 0)
                    {
                        foreach (MailAddress mailAddress in mailAddressList)
                        {
                            oMessage.To.Add(mailAddress);
                        }
                    }

                    //Get CC Mail Address List
                    if (!string.IsNullOrEmpty(cc))
                    {
                        mailAddressList = GetMailAddressList(cc);
                        if (mailAddressList != null && mailAddressList.Count > 0)
                        {
                            foreach (MailAddress mailAddress in mailAddressList)
                            {
                                oMessage.CC.Add(mailAddress);
                            }
                        }
                    }

                    //Get BCC Mail Address List
                    if (!string.IsNullOrEmpty(bcc))
                    {
                        mailAddressList = GetMailAddressList(cc);
                        if (mailAddressList != null && mailAddressList.Count > 0)
                        {
                            foreach (MailAddress mailAddress in mailAddressList)
                            {
                                oMessage.Bcc.Add(mailAddress);
                            }
                        }
                    }

                    if (sSubject != string.Empty)
                        oMessage.Subject = sSubject;

                    oMessage.Body = sBody;
                    oMessage.IsBodyHtml = true;

                    if (oMessage.From != null && oMessage.To != null && oMessage.Body != string.Empty)
                    {
                        oSmtpClient.Send(oMessage);
                        oResult = true;
                    }
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                }

                return oResult;
            }

        }

        private static List<MailAddress> GetMailAddressList(string address)
        {
            List<MailAddress> mailaddressList = new List<MailAddress>();
            if (!string.IsNullOrEmpty(address))
            {
                string[] arrAddress = null;
                if (address.Contains(","))
                {
                    arrAddress = address.Split(',');
                    if (arrAddress.Length > 0)
                    {
                        foreach (string addressSTR in arrAddress)
                        {
                            //oMessage.To.Add(toAddress);
                            MailAddress mailAddress = new MailAddress(addressSTR, "");
                            mailaddressList.Add(mailAddress);

                        }
                    }
                }
                else if (address.Contains(";"))
                {
                    arrAddress = address.Split(';');
                    if (arrAddress.Length > 0)
                    {
                        foreach (string addressSTR in arrAddress)
                        {
                            //oMessage.To.Add(toAddress);
                            MailAddress mailAddress = new MailAddress(addressSTR, "");
                            mailaddressList.Add(mailAddress);
                        }
                    }
                }
                else
                {
                    MailAddress mailAddress = new MailAddress(address, "");
                    mailaddressList.Add(mailAddress);
                }
            }
            return mailaddressList;
        }

        //public static Boolean SendMail(string ToAddress, string ToName, string FromAddress, string EmailSubject, string EmailBody, string cc = "", string bcc = "")
        //{
        //    Boolean oResult = false;

        //    #region
        //    string sNetworkUserName = "";
        //    string sNetworkPassword = "";

        //    if (ConfigurationManager.AppSettings["EmailCredentialUserName"] != null)
        //        sNetworkUserName = ConfigurationManager.AppSettings["EmailCredentialUserName"].ToString();
        //    if (ConfigurationManager.AppSettings["EmailCredentialPassword"] != null)
        //        sNetworkPassword = ConfigurationManager.AppSettings["EmailCredentialPassword"].ToString();

        //    /* Email with Authentication */
        //    if (sNetworkUserName != string.Empty && sNetworkPassword != string.Empty)
        //    {
        //        oSmtpClient.UseDefaultCredentials = true;
        //    }
        //    #endregion

        //    #region Try catch Send mail
        //    try
        //    {
        //        string sFromDisplayName = "";
        //        if (ConfigurationManager.AppSettings["AdminDisplayName"] != null)
        //            sFromDisplayName = ConfigurationManager.AppSettings["AdminDisplayName"].ToString();

        //        MailAddress fromAddress = new MailAddress(FromAddress, "");

        //        //MailMessage oMessage = new MailMessage(fromAddress, toAddress);

        //        MailMessage oMessage = new MailMessage();
        //        oMessage.From = fromAddress;

        //        if (ToAddress.Contains(","))
        //        {
        //            oMessage.To.Add(ToAddress); //To support  multiple emails.

        //        }
        //        else
        //        {
        //            MailAddress toAddress = new MailAddress(ToAddress, ToName);
        //            oMessage.To.Add(toAddress);
        //        }

        //        if (cc != "")
        //        {
        //            oMessage.CC.Add(cc);
        //        }
        //        if (bcc != "")
        //        {
        //            oMessage.Bcc.Add(bcc);
        //        }
        //        if (ConfigurationManager.AppSettings["SendMailToAdmin"].ToStringSafe().ToLower() == "true")
        //        {
        //            oMessage.Bcc.Add(ConfigurationManager.AppSettings["SendMailCopyToAdmin"].ToString());
        //        }

        //        if (EmailSubject != string.Empty)
        //            oMessage.Subject = EmailSubject;

        //        oMessage.Body = EmailBody;
        //        oMessage.IsBodyHtml = true;

        //        if (oMessage.From != null && oMessage.To != null && oMessage.Body != string.Empty)
        //        {

        //            oSmtpClient.Send(oMessage);
        //            oResult = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex);
        //        throw ex;
        //    }
        //    #endregion Try Catch
        //    return oResult;
        //}

        //public static bool GetEmailTemplateAndSendEmail(int intTemplateId, System.Collections.Hashtable hsUserInfo, string templateName, string toAddress, out string statusMessage, out string mailBody)
        //{

        //    bool result = false;
        //    DataTable dtResult = new DataTable();
        //    statusMessage = String.Empty;
        //    mailBody = String.Empty;
        //    string from = string.Empty;
        //    string body = string.Empty;
        //    string subject = string.Empty;
        //    List<EmailTransaction> oEmailTransactions = new List<EmailTransaction>();
        //    try
        //    {
        //        SqlCommand selectCommand = new SqlCommand("GetEmailTemplates");
        //        selectCommand.CommandType = CommandType.StoredProcedure;
        //        selectCommand.Parameters.AddWithValue("@ID", intTemplateId);
        //        selectCommand.Parameters.AddWithValue("@TemplateName", templateName);
        //        dtResult = SqlHelper.GetDataTable(selectCommand);
        //        if (dtResult != null)
        //        {

        //            body = dtResult.Rows[0]["TemplateHeader"].ToString().Replace("''", "'") + dtResult.Rows[0]["TemplateContent"].ToString().Replace("''", "'") + dtResult.Rows[0]["TemplateFooter"].ToString().Replace("''", "'");
        //            from = dtResult.Rows[0]["TemplateFrom"].ToString();
        //            subject = dtResult.Rows[0]["TemplateSubject"].ToString().Replace("''", "'");

        //            foreach (System.Collections.DictionaryEntry de in hsUserInfo)
        //            {
        //                if (de.Key.ToString().Contains("{{$$"))
        //                    body = body.Replace(de.Key.ToString(), de.Value.ToString());
        //            }
        //            bool testingMails = Convert.ToBoolean(ConfigurationManager.AppSettings["SendTestEmail"].ToString());
        //            if (testingMails)
        //            {
        //                toAddress = ConfigurationManager.AppSettings["ToEmail"].ToString();
        //            }
        //            else
        //            {
        //                if (hsUserInfo.ContainsKey("{{$$Email$$}}"))
        //                    toAddress = hsUserInfo["{{$$Email$$}}"].ToString();
        //            }
        //            mailBody = body;
        //            oEmailTransactions = TrackEmailSend(0, hsUserInfo, toAddress, from, body, subject, "",0);

        //            SendMail(toAddress, "", from, subject, body);
        //            result = true;
        //            statusMessage = "Success";
        //        }
        //        else
        //        {
        //            statusMessage = "Template details not found";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Log exception in Exception Logger.
        //        result = false;
        //        statusMessage = "Exception Occured:";
        //        //log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message, ex);
        //        //throw ex;
        //        if (oEmailTransactions != null && oEmailTransactions.Count > 0)
        //            TrackEmailSend(oEmailTransactions[0].ID, hsUserInfo, toAddress, from, body, subject, ex.Message,1);
        //    }

        //    return result;
        //}

        //private static List<EmailTransaction> TrackEmailSend(int ID, System.Collections.Hashtable hsUserInfo, string toAddress, string from, string body, string subject, string ErrorMessage,Int16 status)
        //{
        //    EmailTransactionManager oEmailTransactionManager = new EmailTransactionManager();
        //    EmailTransaction oEmailTransaction = new EmailTransaction();
        //    List<EmailTransaction> oEmailTransactionList = new List<EmailTransaction>();

        //    oEmailTransaction.ID = ID;
        //    oEmailTransaction.FromId = from;
        //    oEmailTransaction.ToId = toAddress;
        //    oEmailTransaction.Subject = subject;
        //    oEmailTransaction.Body = body;
        //    if (hsUserInfo.ContainsKey("{{$$FirstName$$}}"))
        //        oEmailTransaction.FirstName = hsUserInfo["{{$$FirstName$$}}"].ToString();
        //    if (hsUserInfo.ContainsKey("{{$$LastName$$}}"))
        //        oEmailTransaction.LastName = hsUserInfo["{{$$LastName$$}}"].ToString();
        //    oEmailTransaction.ErrorCode = null;
        //    oEmailTransaction.ErrorMessage = ErrorMessage;
        //    oEmailTransaction.SiteID = 5;
        //    oEmailTransaction.MailStatus = status;
        //    oEmailTransactionList = oEmailTransactionManager.Save(oEmailTransaction);
        //    return oEmailTransactionList;
        //}
        #endregion
    }
}
